# High Resolution Data Assimilation of Cardiac Mechanics Applied to a Dyssynchronous Ventricle  #

This repo contains the code that is based on the following paper: 

* Balaban, G., Finsberg, H., Odland, H.H., Rognes, M., Ross, S., Sundnes, J. and Wall, S., 2017. High Resolution Data Assimilation of Cardiac Mechanics Applied to a Dyssynchronous Ventricle. International Journal for Numerical Methods in Biomedical Engineering.

A cardiac computational model is constrained using clinical measurements such as pressure, volume and regional strain. The problem is formulated as a PDE-constrained optimisation problem where the objective functional represents the misfit between measured and simulated data. The control parameter for the active phase is a spatially varying contraction parameter defined at every vertex in the mesh. The control parameters for the passive phase is the linear isotropic material parameter from a Holzapfel and Ogden transversally isotropic material. The problem is solved using a gradient based optimization algorithm where the gradient is provided by solving the adjoint system


## Requirements ##
In order to simply run the code you need
```
* FEniCS version 2016.1
  -- http://fenicsproject.org
* Dolfin-Adjoint version 2016.1
  -- http://www.dolfin-adjoint.org/en/latest/download/index.html
* yaml
  -- Used for storing parameters
  -- pip install pyyaml
```

### How to run it ###
Simply do
```
python demo.py
```
This will run the code using the patient data described in the paper which is located in the folder "data".
In the same script you can change parameters as you like.
The results will be stored in a new folder named "results".
The code also runs i parallel using `mpirun -n x. 

### Reproduce the results ###

It is possible to run simulations and changing the parameters in order to generate the same results as the ones presented in the paper. You can also reproduce the figures by running the script `reproduce_figures.sh`, which will download the results and generate the figures. 

## Documentation ##
Generate documentation:
```
cd docs
make html
``` 
Open `_build/html/index.html` in your favourite web-browser.


## License ##
CARDIAC_HIGHRES_DATAASSIM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CARDIAC_HIGHRES_DATAASSIM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with CARDIAC_HIGHRES_DATAASSIM. If not, see <http://www.gnu.org/licenses/>.

The clinical data in this repository is obtained with permission from the Oslo University Hospital.
If you plan to publish results using any of the clinical data in your research,
please contact the author for approval. 


## Citing ##
If you plan to use the code in your research we would be grateful if you could cite the following publication:

* Balaban, G., Finsberg, H., Odland, H.H., Rognes, M., Ross, S., Sundnes, J. and Wall, S., 2017. High Resolution Data Assimilation of Cardiac Mechanics Applied to a Dyssynchronous Ventricle. International Journal for Numerical Methods in Biomedical Engineering.

## Contributors ##
* Henrik Finsberg (henriknf@simula.no)
* Gabriel Balaban (gabrib@simula.no)