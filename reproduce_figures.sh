#!/usr/bin/env bash

mkdir results
cd results
wget https://www.dropbox.com/s/q3ierdubfljcbw6/results.tar
tar -xvf results.tar
rm results.tar
cd ..
python postprocess.py
