#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of PULSE-ADJOINT.
#
# PULSE-ADJOINT is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE-ADJOINT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE-ADJOINT. If not, see <http://www.gnu.org/licenses/>.
from cardiac_highres_dataassim.demo import main_patient, main_synthetic, setup_adjoint_contraction_parameters

def main(params):

    if params["synth_data"]:
        main_synthetic(params)
        
    else:
        main_patient(params)

   

if __name__=="__main__":
    import yaml, sys, shutil

    infile = sys.argv[1]
    outfile = sys.argv[2]
    
    with open(infile, 'rb') as parfile:
        params_dict = yaml.load(parfile)

    params = setup_adjoint_contraction_parameters()
   
    params.update(params_dict)
    assert outfile == params["sim_file"]

    shutil.copy(infile, params["outdir"] + "/input.yml")
        


    main(params)

  
    
